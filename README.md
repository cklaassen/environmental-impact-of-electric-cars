# Environmental Impact Of Electric Cars

Have you ever been confused regarding the _true_ efficiency of an electric car?
The internal combustion engine has been around for my entire life, and thus I have gained a strong understanding of **miles per gallon**.
Looking through different manufacturer's websites, they all list **Watt hours per mile** as their efficiency metric.  To be able to gain insight into this metric,
I designed the following calculator.  It calculates the **mpg** of your electric car based on the state you are in.

## Usage

The executable file within this repository is `main.py`.  Run this file as well as follow the terminal prompts to run the program.

### Dependencies

- `pandas`

## Description

In this section I will explain, in depth, each of the following calculations performed to arrive at a relative **mpg**.
If you disagree with my methodology, feel free to reach out.

### Datasets

- `annual_generation_state.csv:`
  - *Origin*: [XLS dataset](https://www.eia.gov/electricity/data/state/annual_generation_state.xls)
  - *Source*: [U.S. Energy Information Administration](https://www.eia.gov)
  - *Contents*: Energy generation (in Megawatthours) per state as well as energy source

- `emission_annual.csv:`
  - *Origin*: [XLSX dataset](https://www.eia.gov/electricity/data/state/emission_annual.xlsx)
  - *Source*: [U.S. Energy Information Administration](https://www.eia.gov)
  - *Contents*: Net pollution of CO<sub>2</sub>, SO<sub>2</sub>, and NO<sub>x</sub> (all units in Metric Tons) per state as well as energy source

- `ev-database.csv:`
  - *Origin*: [ev-database](https://ev-database.org)
  - *Source*: [Electric Vehicle Database](https://ev-database.org)
  - *Contents*: A centralized database of the different electric cars (released or announced) and their different characteristics

### Data Processing Pipeline

The first step of the pipeline is to calculate the percentage of each energy source the selected state is using.
This is performed by taking the ratio of electrical production via the `annual_generation_state.csv`.  By calculating
`MWh of Energy Source / Total MWh` we can arrive at the percentage of energy generated in that state that was produced 
by the selected energy source.

The second step of the pipeline is to figure out the defining characteristics of the selected electric vehicle.  The only 
characteristic we are interested in for these calculations are the manufacturers published efficiency.  The attached dataset
publishes this in `Wh / km`, which we can use for this.  As this data is in the metric system, the following data will
all be processed in the metric system and then converted to the imperial system at the end.

The final step of the data processing pipeline is the most involved.  Through a combination of the two EIA datasets, we can come to a metric of `Metric Tons / MWh`, however for
the further calculations we need to convert it into `kg / Wh`, which is trivial (`(Metric Tons * 1,000) / (MWh * 1,000,000)`).
Once we have our relative `kg / Wh` of each energy source.  By summing `(kg / Wh) * % use` we can get a strong estimate for the
typical CO<sub>2</sub>, SO<sub>2</sub>, and NO<sub>x</sub> within a specific state.

### Data Calculation

Once the data processing pipeline has finished, we have all the data required to calculate the relative **mpg** of an
electric car.

By multiplying the electric car's efficiency with the state's estimated polution per *Wh*, we can get
`kg / km`, or the kilograms of a pollutant per kilometer.  According to [Natural Resources Canada](https://www.nrcan.gc.ca/home),
1 liter of gasoline produces 2.3 kg of CO<sub>2</sub>.  Using this metric we can get an excellent estimate of an electric car's emissions
(in `L / km`).  Liters per kilometer can easily be converted to mpg using the conversion
ratio `1 L/km : 2.35214583 mpg`.

That's it!  We have our relative *mpg* of an electric car!

## Future Development

Currently, within my data processing pipeline I get CO<sub>2</sub>, SO<sub>2</sub>, and NO<sub>x</sub>, however in my
calculations I only incorporate CO<sub>2</sub>.  To be able to incorporate the other two molecules released by electrical production,
I looked into using the molecules Global Warming Potentials (GWP), however since both SO<sub>2</sub> and NO<sub>x</sub> are indirect
greenhouse gasses, they do not have readily available data on their GWPs.

While both SO<sub>2</sub> and NO<sub>x</sub> account for a small subset of the impact of electricity production, 
the current calculations cannot be considered perfect until they are incorporated. 

In addition to improving the accuracy of the **mpg** metric, the calculations performed in this script might be slightly
overcomplicated.  The EIA dataset used to perform these calculations presents both a total *MWh* and a total *Metric Tons*
of emissions.  Through these two values alone you should be able to calculate the general net pollution of generating 1 Wh of
energy in a given state.  This should both simplify understanding of the script, as well as improve runtime of the program
as the net runtime will decrease from `O(n)` to `O(1)`.
