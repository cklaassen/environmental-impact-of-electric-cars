def calculate_efficiency(impact: dict, car_efficiency: int):
    singular_impact = impact['CO2 (kg/Wh)']
    return (2.3 / (car_efficiency * singular_impact)) * 2.35215
