import pandas as pd


def _parse_information(df: pd.DataFrame, state: str) -> pd.DataFrame:
    df = df[(df['Year'] == 2020) & (df['Producer Type'].str.contains("Total")) & (df['State'] == state)]
    df['CO2\n(Metric Tons)'] = pd.to_numeric(df['CO2\n(Metric Tons)'].str.replace(",", ""))
    df['SO2\n(Metric Tons)'] = pd.to_numeric(df['SO2\n(Metric Tons)'].str.replace(",", ""))
    df['NOx\n(Metric Tons)'] = pd.to_numeric(df['NOx\n(Metric Tons)'].str.replace(",", ""))
    return df


class Emissions:

    def __init__(self, state: str, state_electric: pd.DataFrame):
        self.state_emission = _parse_information(pd.read_csv('./data/emission_annual.csv'), state)
        self.state_electricity = state_electric

    def emission_impact(self, emissions: dict):
        data = {
            'CO2\n(Metric Tons)': 0,
            'SO2\n(Metric Tons)': 0,
            'NOx\n(Metric Tons)': 0
        }

        for i, row in self.state_emission.iterrows():
            if row['Energy Source'] == 'All Sources':
                continue
            for key in data:
                """
                row[key] == Metric Tons
                state_electricity == MWh
                
                row[key] * 1000 == kilograms
                state_electricity * 1000000 == Wh
                """
                data[key] += (row[key] * 1000
                              / (self.state_electricity[self.state_electricity['ENERGY SOURCE'] == row['Energy Source']]['GENERATION (Megawatthours)'].iloc[0] * 1000000)) \
                             * emissions[row['Energy Source']]
        return {key[:3] + ' (kg/Wh)': data[key] for key in data}
