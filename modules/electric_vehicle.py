import pandas as pd


class EV:

    def __init__(self):
        self.ev_data = pd.read_csv('./data/ev-database.csv')
        while True:
            ev = input("Enter in your vehicle name (enter `list` for a list of compatible ev's): ")
            if ev == 'list':
                for vehicle in self.ev_data['Name']:
                    print(vehicle)
            elif ev not in self.ev_data['Name'].tolist():
                print("Sorry, `" + ev + "` is not in our database!")
            else:
                self.ev = ev
                break
        self.efficiency = self.ev_data[self.ev_data['Name'] == ev]['Efficiency'].iloc[0]
