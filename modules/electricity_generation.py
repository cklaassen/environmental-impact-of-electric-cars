import warnings
import pandas as pd
from pandas.core.common import SettingWithCopyWarning
warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)


def _parse_information(df: pd.DataFrame, state: str) -> pd.DataFrame:
    df = df[(df['YEAR'] == 2020) & (df['TYPE OF PRODUCER'].str.contains("Total")) & (df['STATE'] == state)]
    df['GENERATION (Megawatthours)'] = pd.to_numeric(df['GENERATION (Megawatthours)'].str.replace(",", ""))
    return df


def _percentages(df: pd.DataFrame) -> dict:
    total = df[df['ENERGY SOURCE'] == 'Total']['GENERATION (Megawatthours)'].iloc[0]
    return {source: generation / total
            for source, generation in zip(df['ENERGY SOURCE'], df['GENERATION (Megawatthours)'])}


class Electricity:

    def __init__(self, state):
        self.electricity_generation_data = _parse_information(pd.read_csv('./data/annual_generation_state.csv'), state)
        self.electricity_percentages = _percentages(self.electricity_generation_data)
