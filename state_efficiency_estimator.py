import pandas as pd

a = pd.read_csv('./data/annual_generation_state.csv')
b = pd.read_csv('./data/emission_annual.csv')

a = a[a['YEAR'] == 2020]
b = b[b['Year'] == 2020]

a = a[a['ENERGY SOURCE'] == 'Total']
b = b[b['Energy Source'].str.contains('All')]

a = a[a['TYPE OF PRODUCER'].str.contains('Total')]
b = b[b['Producer Type'].str.contains('Total')]

data = []
for state in a['STATE']:
    print(state)
    if len(a[a['STATE'] == state]['GENERATION (Megawatthours)']) > 0 and len(b[b['State'] == state]['CO2\n(Metric Tons)']) > 0:
        data.append([state, int(b[b['State'] == state]['CO2\n(Metric Tons)'].str.replace(',', '')) / int(a[a['STATE'] == state]['GENERATION (Megawatthours)'].str.replace(',', ''))])

print(sorted(data, key=lambda x: x[1]))
