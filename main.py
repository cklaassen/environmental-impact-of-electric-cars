from modules.electricity_generation import Electricity
from modules.electric_vehicle import EV
from modules.emissions import Emissions
from modules.efficiency_calculator import calculate_efficiency

state = input("Please input your USPS state abbreviation (e.g. California -> CA): ")
e = Electricity(state)
vehicle = EV()
em = Emissions(state, e.electricity_generation_data)
impact = em.emission_impact(e.electricity_percentages)

net_efficiency = calculate_efficiency(impact, int(vehicle.efficiency.split(' ')[0]))

print("\nYou get the equivalent of " + str(round(net_efficiency, 2)) + " mpg!")
